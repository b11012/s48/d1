// Mock database
let posts = [];
// count variable that will server as the post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
		// prevents the page from loading
		e.preventDefault();

		posts.push({
			id: count,
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value
		});

		console.log(count);
	// Increment the id value in count variable
	count++;

	showPosts(posts);
	alert('Successfully added.');

});

// Show posts.
const showPosts = (posts) => {
	// Sets the html structure to display new posts
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})


	document.querySelector('#div-post-entries').innerHTML = postEntries
}

// Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

		e.preventDefault();

		/*	
			posts[0].id = 1;
			posts[0].title = Inception;
			posts[0].body =Scifi;
			
			txt-edit-id = 1
			txt-edit-title = Anabelle;
			txt-edit-body = Horror
			posts[0].id = 1 === #txt-edit-id.value = 1

			posts.length = 3;
			posts[i] == 0 .id = 1
		*/

		for(let i = 0; i < posts.length; i++) {
			if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
				posts[i].title = document.querySelector('#txt-edit-title').value;
				posts[i].body = document.querySelector('#txt-edit-body').value;

				showPosts(posts);
				alert('Successfully updated');

				break;
			}
		}
})


// DELETE POST

const deletePost = (id) => {
	posts = posts.filter((post) => {

		if (post.id.toString() !== id) {
			return post;
		}
	})

	// remove the post from view only
	document.querySelector(`#post-${id}`).remove();
	console.log(posts);
}
